package com;

import java.lang.reflect.Method;
import java.nio.file.Files;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.junit.jupiter.api.TestInfo;
import org.springframework.core.io.ClassPathResource;

// Do something more sophisticated in a real project ;)
// (a custom annotation argument provider for example)
@UtilityClass
public class TestFileUtils {

  public static String loadRequestFrom(final TestInfo testInfo) {
    // Same for requests
    return "";
  }

  @SneakyThrows
  public static String loadResponseFrom(final TestInfo testInfo) {
    return Files.readString(new ClassPathResource(
        "/acceptance/response/" + testInfo.getTestMethod().map(Method::getName).orElse("")
            + ".json").getFile()
        .toPath());
  }
}
