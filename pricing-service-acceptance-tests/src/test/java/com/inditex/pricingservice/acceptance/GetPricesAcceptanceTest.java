package com.inditex.pricingservice.acceptance;

import static com.TestFileUtils.loadResponseFrom;

import com.inditex.pricingservice.infrastructure.PricingServiceApplication;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;

@AutoConfigureWebTestClient
@SpringBootTest(classes = PricingServiceApplication.class)
public class GetPricesAcceptanceTest {

  private static final long PRODUCT_ID = 35_455L;

  private static final long BRAND_ID = 1;

  @Autowired
  private WebTestClient webClient;

  @Test
  void price_at_10_on_the_14th(final TestInfo testInfo) {
    // When
    webClient.get().uri(uriBuilder ->
            uriBuilder
                .path("/prices")
                .queryParam("product-id", PRODUCT_ID)
                .queryParam("brand-id", BRAND_ID)
                .queryParam("date", "2020-06-14T10:00:00.00Z")
                .build()).exchange()

        // Then
        .expectStatus().isOk()
        .expectBody().json(loadResponseFrom(testInfo));
  }

  @Test
  void price_at_16_on_the_14th(final TestInfo testInfo) {
    // When
    webClient.get().uri(uriBuilder ->
            uriBuilder
                .path("/prices")
                .queryParam("product-id", PRODUCT_ID)
                .queryParam("brand-id", BRAND_ID)
                .queryParam("date", "2020-06-14T16:00:00.00Z")
                .build()).exchange()

        // Then
        .expectStatus().isOk()
        .expectBody().json(loadResponseFrom(testInfo));
  }

  @Test
  void price_at_21_on_the_14th(final TestInfo testInfo) {
    // When
    webClient.get().uri(uriBuilder ->
            uriBuilder
                .path("/prices")
                .queryParam("product-id", PRODUCT_ID)
                .queryParam("brand-id", BRAND_ID)
                .queryParam("date", "2020-06-14T21:00:00.00Z")
                .build()).exchange()

        // Then
        .expectStatus().isOk()
        .expectBody().json(loadResponseFrom(testInfo));
  }

  @Test
  void price_at_10_on_the_15th(final TestInfo testInfo) {
    // When
    webClient.get().uri(uriBuilder ->
            uriBuilder
                .path("/prices")
                .queryParam("product-id", PRODUCT_ID)
                .queryParam("brand-id", BRAND_ID)
                .queryParam("date", "2020-06-15T10:00:00.00Z")
                .build()).exchange()

        // Then
        .expectStatus().isOk()
        .expectBody().json(loadResponseFrom(testInfo));
  }

  @Test
  void price_at_21_on_the_16th(final TestInfo testInfo) {
    // When
    webClient.get().uri(uriBuilder ->
            uriBuilder
                .path("/prices")
                .queryParam("product-id", PRODUCT_ID)
                .queryParam("brand-id", BRAND_ID)
                .queryParam("date", "2020-06-16T21:00:00.00Z")
                .build()).exchange()

        // Then
        .expectStatus().isOk()
        .expectBody().json(loadResponseFrom(testInfo));
  }
}
