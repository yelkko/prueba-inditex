package com.inditex.pricingservice.application.usecases;

import com.inditex.pricingservice.application.model.Price;
import com.inditex.pricingservice.application.model.Prices;
import java.time.Instant;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class GetPrices {

  private final Prices prices;

  public Mono<Price> execute(final long productId, final int brandId, final Instant date) {
    return prices.searchPrice(productId, brandId, date);
  }
}
