package com.inditex.pricingservice.application.model;

import java.time.Instant;
import reactor.core.publisher.Mono;

public interface Prices {

  Mono<Price> searchPrice(long productId, int brandId, Instant date);
}
