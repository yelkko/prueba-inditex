package com.inditex.pricingservice.application.model;

import java.time.Instant;

public record Price(
    Long id,
    Long productId,
    Integer brandId,
    Integer priceList,
    Instant startDate,
    Instant endDate,
    Amount amount
) {

}
