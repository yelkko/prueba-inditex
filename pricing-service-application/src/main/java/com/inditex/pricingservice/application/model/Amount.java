package com.inditex.pricingservice.application.model;

import java.math.BigDecimal;

public record Amount(BigDecimal value, String currency) {

}
