package com.inditex.pricingservice.application.usecases;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static reactor.test.StepVerifier.create;

import com.inditex.pricingservice.application.model.Amount;
import com.inditex.pricingservice.application.model.Price;
import com.inditex.pricingservice.application.model.Prices;
import java.math.BigDecimal;
import java.time.Instant;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
class GetPricesTest {

  @Mock
  private Prices prices;

  @InjectMocks
  private GetPrices getPrices;

  @Test
  void givenExistingPrice_whenExecute_thenReturnPrices() {

    // Given
    final var samplePrice = fixedSamplePrice();
    given(prices.searchPrice(anyLong(), anyInt(), any())).willReturn(Mono.just(samplePrice));

    // When
    final var publisher = getPrices.execute(1, 3, Instant.parse("2022-12-03T10:15:30.00Z"));

    // Then
    create(publisher).expectNext(samplePrice).verifyComplete();
  }

  private Price fixedSamplePrice() {
    return new Price(1L, 2L, 3, 4, Instant.parse("2021-12-03T10:15:30.00Z"),
        Instant.parse("2023-12-03T10:15:30.00Z"), new Amount(BigDecimal.TEN, "EUR"));
  }

}