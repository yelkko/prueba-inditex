package com.inditex.pricingservice.infrastructure.persistence.model;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static reactor.test.StepVerifier.create;

import com.inditex.pricingservice.application.model.Amount;
import com.inditex.pricingservice.application.model.Price;
import com.inditex.pricingservice.infrastructure.persistence.mapper.PricePersistenceMapper;
import com.inditex.pricingservice.infrastructure.persistence.repository.JpaPriceRepository;
import java.math.BigDecimal;
import java.time.Instant;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
class JpaPricesTest {

  @Mock
  private JpaPriceRepository repository;

  // I know spy can be a smell, but I prefer to keep the code cleaner rather than use static mappers
  @Spy
  private PricePersistenceMapper mapper = Mappers.getMapper(PricePersistenceMapper.class);

  @InjectMocks
  private JpaPrices jpaPrices;

  @Test
  void givenExistingPrice_whenSearchPrice_thenReturnThePrice() {

    // Given
    final var sampleJpaPrice = fixedSampleJpaPrice();
    given(repository.searchPrice(anyLong(), anyInt(), any())).willReturn(Mono.just(sampleJpaPrice));

    // When
    final var publisher = jpaPrices.searchPrice(1L, 1, Instant.now());

    // Then
    create(publisher).expectNext(expectedPrice()).verifyComplete();
  }

  private JpaPrice fixedSampleJpaPrice() {
    final var jpaPrice = new JpaPrice();
    jpaPrice.setId(1L);
    jpaPrice.setProductId(2L);
    jpaPrice.setBrandId(3);
    jpaPrice.setPriceList(4);
    jpaPrice.setStartDate(Instant.parse("2021-12-03T10:15:30.00Z"));
    jpaPrice.setEndDate(Instant.parse("2023-12-03T10:15:30.00Z"));
    jpaPrice.setPrice(BigDecimal.TEN);
    jpaPrice.setCurrency("BTC");
    return jpaPrice;
  }

  private Price expectedPrice() {
    return new Price(1L, 2L, 3, 4, Instant.parse("2021-12-03T10:15:30.00Z"),
        Instant.parse("2023-12-03T10:15:30.00Z"), new Amount(BigDecimal.TEN, "BTC"));
  }

}