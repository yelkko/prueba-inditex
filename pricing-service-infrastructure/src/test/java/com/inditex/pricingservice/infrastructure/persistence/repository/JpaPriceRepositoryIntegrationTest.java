package com.inditex.pricingservice.infrastructure.persistence.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static reactor.test.StepVerifier.create;

import java.math.BigDecimal;
import java.time.Instant;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;

// Here I would have liked to add the data for the test using @Sql but, it is not yet
// well-supported in reactive and some things would have to be implemented, there is always the
// possibility to load it by hand from a file, but I decided to use the local data.
@DataR2dbcTest
class JpaPriceRepositoryIntegrationTest {

  @Autowired
  private JpaPriceRepository jpaPriceRepository;

  @Test
  void givenExistingPriceForGivenParameters_whenSearch_thenReturnPrice() {
    final var publisher = jpaPriceRepository.searchPrice(35455, 1,
        Instant.parse("2020-06-14T00:00:00.00Z"));

    create(publisher).assertNext(actual -> {
      assertThat(actual).isNotNull();
      assertThat(actual.getId()).isEqualTo(1L);
      assertThat(actual.getProductId()).isEqualTo(35455L);
      assertThat(actual.getBrandId()).isEqualTo(1);
      assertThat(actual.getPriceList()).isEqualTo(1);
      assertThat(actual.getPrice()).isEqualTo(new BigDecimal("35.50"));
      assertThat(actual.getPriority()).isEqualTo(0);
      assertThat(actual.getStartDate()).isEqualTo(Instant.parse("2020-06-14T00:00:00Z"));
      assertThat(actual.getEndDate()).isEqualTo(Instant.parse("2020-12-31T23:59:59Z"));
    }).verifyComplete();
  }

  @Test
  void givenMultiplePricesForGivenParameters_whenSearch_thenReturnPriceWithHighestPriority() {
    final var publisher = jpaPriceRepository.searchPrice(35455L, 1,
        Instant.parse("2020-06-15T00:01:00.00Z"));

    create(publisher).assertNext(actual -> {
      assertThat(actual).isNotNull();
      assertThat(actual.getId()).isEqualTo(3L);
      assertThat(actual.getProductId()).isEqualTo(35455L);
      assertThat(actual.getBrandId()).isEqualTo(1);
      assertThat(actual.getPriceList()).isEqualTo(3);
      assertThat(actual.getPrice()).isEqualTo(new BigDecimal("30.50"));
      assertThat(actual.getPriority()).isEqualTo(1);
      assertThat(actual.getStartDate()).isEqualTo(Instant.parse("2020-06-15T00:00:00Z"));
      assertThat(actual.getEndDate()).isEqualTo(Instant.parse("2020-06-15T11:00:00Z"));
    }).verifyComplete();
  }

  @Test
  void givenNotExistingPriceForGivenParameters_whenSearch_thenComplete() {
    final var publisher = jpaPriceRepository.searchPrice(35455, 1,
        Instant.parse("1999-06-14T00:00:00.00Z"));

    create(publisher).verifyComplete();
  }
}