package com.inditex.pricingservice.infrastructure.api.controller;

import static java.nio.file.Files.readString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;

import com.inditex.pricingservice.application.model.Amount;
import com.inditex.pricingservice.application.model.Price;
import com.inditex.pricingservice.application.usecases.GetPrices;
import com.inditex.pricingservice.infrastructure.api.mapper.PriceApiMapper;
import java.math.BigDecimal;
import java.time.Instant;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

@WebFluxTest(GetPriceController.class)
class GetPriceControllerIntegrationTest {

  @Value("classpath:integration/get_price_controller/ok-response.json")
  private Resource expectedOkResponse;

  @Value("classpath:integration/get_price_controller/internal-error-response.json")
  private Resource expectedInternalErrorResponse;

  @Value("classpath:integration/get_price_controller/bad-request-response.json")
  private Resource expectedBadRequestResponse;

  @MockBean
  private GetPrices getPrices;

  @Autowired
  private WebTestClient webClient;

  @Test
  void givenPrice_getPrice_shouldReturnOkResponseWithThePrice() throws Exception {

    // Given
    given(getPrices.execute(anyLong(), anyInt(), any())).willReturn(Mono.just(fixedSamplePrice()));

    // When
    webClient.get().uri(uriBuilder ->
            uriBuilder
                .path("/prices")
                .queryParam("product-id", 1)
                .queryParam("brand-id", 2)
                .queryParam("date", "2021-12-04T10:15:30.00Z")
                .build()).exchange()

        // Then
        .expectStatus().isOk()
        .expectBody().json(readString(expectedOkResponse.getFile().toPath()));
  }


  // Perhaps it is questionable whether this has to return an Ok with an empty body,
  // it could be discussed.
  @Test
  void givenNoPrice_getPrice_shouldReturnOkResponseWithEmptyBody() throws Exception {

    // Given
    given(getPrices.execute(anyLong(), anyInt(), any())).willReturn(Mono.empty());

    // When
    webClient.get().uri(uriBuilder ->
            uriBuilder
                .path("/prices")
                .queryParam("product-id", 1)
                .queryParam("brand-id", 2)
                .queryParam("date", "2021-12-04T10:15:30.00Z")
                .build()).exchange()

        // Then
        .expectStatus().isOk()
        .expectBody().isEmpty();
  }

  @Test
  void givenNotAllRequiredParameters_getPrice_shouldReturnBadRequest()
      throws Exception {

    // Given
    given(getPrices.execute(anyLong(), anyInt(), any())).willReturn(Mono.empty());

    // When
    webClient.get().uri(uriBuilder ->
            uriBuilder
                .path("/prices")
                .queryParam("brand-id", 2)
                .queryParam("date", "2021-12-04T10:15:30.00Z")
                .build()).exchange()

        // Then
        .expectStatus().isBadRequest()
        .expectBody().json(readString(expectedBadRequestResponse.getFile().toPath()));
  }

  // This test doesn't really make much sense because we do not have any defined error case,
  // but I add it to show an example of an error.
  @Test
  void givenRuntimeException_getPrice_shouldReturnInternalServerError() throws Exception {

    // Given
    given(getPrices.execute(anyLong(), anyInt(), any()))
        .willReturn(Mono.error(new RuntimeException("test internal error")));

    // When
    webClient.get().uri(uriBuilder ->
            uriBuilder
                .path("/prices")
                .queryParam("product-id", 1)
                .queryParam("brand-id", 2)
                .queryParam("date", "2021-12-04T10:15:30.00Z")
                .build()).exchange()

        // Then
        .expectStatus().is5xxServerError()
        .expectBody().json(readString(expectedInternalErrorResponse.getFile().toPath()));
  }

  private Price fixedSamplePrice() {
    return new Price(1L, 2L, 3, 4, Instant.parse("2021-12-03T10:15:30.00Z"),
        Instant.parse("2023-12-03T10:15:30.00Z"), new Amount(BigDecimal.ONE, "GBP"));
  }

  @TestConfiguration
  static class TestConfig {

    @Bean
    public PriceApiMapper priceApiMapper() {
      return Mappers.getMapper(PriceApiMapper.class);
    }
  }
}