package com.inditex.pricingservice.infrastructure.api.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static reactor.test.StepVerifier.create;

import com.inditex.pricingservice.application.model.Amount;
import com.inditex.pricingservice.application.model.Price;
import com.inditex.pricingservice.application.usecases.GetPrices;
import com.inditex.pricingservice.dto.AmountDTO;
import com.inditex.pricingservice.dto.PriceDTO;
import com.inditex.pricingservice.infrastructure.api.mapper.PriceApiMapper;
import java.math.BigDecimal;
import java.time.Instant;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
class GetPriceControllerTest {

  @Mock
  private GetPrices useCase;

  // I know spy can be a smell, but I prefer to keep the code cleaner rather than use static mappers
  @Spy
  private PriceApiMapper mapper = Mappers.getMapper(PriceApiMapper.class);

  @InjectMocks
  private GetPriceController controller;

  @Test
  void givenExistingPrice_whenGetPrice_thenThePriceIsReturned() {

    // Given
    final var samplePrice = fixedSamplePrice();
    given(useCase.execute(anyLong(), anyInt(), any())).willReturn(Mono.just(samplePrice));

    // When
    final var publisher = controller.getPrice(Instant.now(), 1L, 1, null);

    // Then
    create(publisher).expectNext(okResponse()).verifyComplete();
  }

  private Price fixedSamplePrice() {
    return new Price(1L, 2L, 3, 4, Instant.parse("2021-12-03T10:15:30.00Z"),
        Instant.parse("2023-12-03T10:15:30.00Z"), new Amount(BigDecimal.ONE, "GBP"));
  }

  private ResponseEntity<PriceDTO> okResponse() {
    return ResponseEntity.ok(new PriceDTO(2L, 3, 4, Instant.parse("2021-12-03T10:15:30.00Z"),
        Instant.parse("2023-12-03T10:15:30.00Z"), new AmountDTO(BigDecimal.ONE, "GBP")));
  }
}