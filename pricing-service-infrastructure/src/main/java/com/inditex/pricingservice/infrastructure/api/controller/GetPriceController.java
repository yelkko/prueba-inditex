package com.inditex.pricingservice.infrastructure.api.controller;

import com.inditex.pricingservice.api.GetPriceApi;
import com.inditex.pricingservice.application.usecases.GetPrices;
import com.inditex.pricingservice.dto.PriceDTO;
import com.inditex.pricingservice.infrastructure.api.mapper.PriceApiMapper;
import java.time.Instant;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class GetPriceController implements GetPriceApi {

  private final GetPrices getPrices;

  private final PriceApiMapper priceMapper;

  @Override
  public Mono<ResponseEntity<PriceDTO>> getPrice(final Instant date, final Long productId,
      final Integer brandId, final ServerWebExchange exchange) {
    return getPrices.execute(productId, brandId, date)
        .map(priceMapper::toDto)
        .map(ResponseEntity::ok);
  }

}
