package com.inditex.pricingservice.infrastructure.persistence.mapper;

import com.inditex.pricingservice.application.model.Price;
import com.inditex.pricingservice.infrastructure.persistence.model.JpaPrice;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface PricePersistenceMapper {

  @Mapping(target = "amount.value", source = "price")
  @Mapping(target = "amount.currency", source = "currency")
  Price toDomain(JpaPrice price);
}
