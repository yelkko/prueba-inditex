package com.inditex.pricingservice.infrastructure.persistence.repository;

import com.inditex.pricingservice.infrastructure.persistence.model.JpaPrice;
import java.time.Instant;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Mono;

public interface JpaPriceRepository extends R2dbcRepository<JpaPrice, Long> {

  @Query("""
      SELECT p.*
      FROM prices p
      WHERE p.product_id = :productId
        AND p.brand_id = :brandId
        AND :date >= p.start_date
        AND :date <= p.end_date
      ORDER BY p.priority DESC
      LIMIT 1
      """)
  Mono<JpaPrice> searchPrice(long productId, int brandId, Instant date);
}
