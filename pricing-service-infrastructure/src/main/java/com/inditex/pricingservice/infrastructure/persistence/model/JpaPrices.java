package com.inditex.pricingservice.infrastructure.persistence.model;

import com.inditex.pricingservice.application.model.Price;
import com.inditex.pricingservice.application.model.Prices;
import com.inditex.pricingservice.infrastructure.persistence.mapper.PricePersistenceMapper;
import com.inditex.pricingservice.infrastructure.persistence.repository.JpaPriceRepository;
import java.time.Instant;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class JpaPrices implements Prices {

  private final JpaPriceRepository jpaPriceRepository;

  private final PricePersistenceMapper priceMapper;

  @Override
  public Mono<Price> searchPrice(final long productId, final int brandId, final Instant date) {
    return jpaPriceRepository
        .searchPrice(productId, brandId, date)
        .map(priceMapper::toDomain);
  }
}
