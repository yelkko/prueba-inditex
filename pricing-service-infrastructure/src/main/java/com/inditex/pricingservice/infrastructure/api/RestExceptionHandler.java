package com.inditex.pricingservice.infrastructure.api;

import com.inditex.pricingservice.dto.ErrorDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ServerWebInputException;

@Slf4j
@RestControllerAdvice
class RestExceptionHandler {

  // This is just a small example, here we should have defined and fine-tuned the exceptions,
  // mapping also possible domain errors etc. We should also evaluate if this is the best approach
  // (in my opinion, it is when it comes to small/medium projects because this avoids repetitions).

  @ExceptionHandler(ServerWebInputException.class)
  public ResponseEntity<ErrorDTO> handleServerWebInputException(
      final ServerWebInputException ex) {
    final var error = new ErrorDTO("ITX-0001", ex.getReason());
    return ResponseEntity.badRequest().body(error);
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<ErrorDTO> handleUnexpectedException(
      final Exception ex) {
    final var error = new ErrorDTO("ITX-0000", ex.getMessage());

    log.error("Unexpected exception", ex);

    return ResponseEntity.internalServerError().body(error);
  }
}
