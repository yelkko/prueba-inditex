package com.inditex.pricingservice.infrastructure.api.mapper;

import com.inditex.pricingservice.application.model.Price;
import com.inditex.pricingservice.dto.PriceDTO;
import org.mapstruct.Mapper;

@Mapper
public interface PriceApiMapper {

  PriceDTO toDto(Price price);

}
