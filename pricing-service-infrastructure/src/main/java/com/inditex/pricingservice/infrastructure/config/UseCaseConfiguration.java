package com.inditex.pricingservice.infrastructure.config;

import com.inditex.pricingservice.application.model.Prices;
import com.inditex.pricingservice.application.usecases.GetPrices;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UseCaseConfiguration {

  @Bean
  public GetPrices getPrices(final Prices prices) {
    return new GetPrices(prices);
  }
}
