package com.inditex.pricingservice.infrastructure.persistence.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import java.math.BigDecimal;
import java.time.Instant;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "prices")
public class JpaPrice {

  @Id
  @Include
  @Column("price_id")
  private Long id;

  private Long productId;

  private Integer brandId;

  private Integer priceList;

  private Instant startDate;

  private Instant endDate;

  private Integer priority;

  private BigDecimal price;

  @Column("curr")
  private String currency;

}
